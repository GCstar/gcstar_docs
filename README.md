# GCstar documentation

This project is used to generate the  [documentation website](https://gcstar.gitlab.io/gcstar_docs). It complements the manual that is installed with GCstar on Linux and MacOS systems.

This project is accepting [contributions](Contributing.md) to fix, improve or complement the documentation.
