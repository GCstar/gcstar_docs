+++
title = "Installation"
description = ""
date = "2022-09-27T19:20:24+02:00"
weight = 2
+++

{{%notice info%}}Les versions de **GCstar** disponibles dans les dépots de nombreux systèmes Linux sont dépassées. Dans ces cas, il faut passer par une installation manuelle. {{%/notice%}}

Les différentes versions de GCstar sont disponibles dans le [registre de paquets du projet](https://gitlab.com/GCstar/GCstar/-/packages) sur Gitlab. La dernière version publiée est la [1.8.0](https://gitlab.com/GCstar/GCstar/-/packages/13986875), le registre contient aussi des paquets et installeurs pour la version en cours de développement (latest).

# Windows 

Téléchargez le fichier **.exe** et exécutez le sur votre système pour lancer le programme d'installation.

# Linux

Consultez la [liste de versions GCstar](https://repology.org/project/gcstar/versions) pour savoir si le logiciel n'est pas installable via les mécanismes natifs de votre environnement.

Sinon, téléchargez depuis le [registre Gitlab](https://gitlab.com/GCstar/GCstar/-/packages) le paquet correspondant à votre type de distribution s'il est disponible (.deb pour Debian/Ubuntu et dérivés, .rpm pour Fedora et dérivés). Exécutez directment l'application gestionnaire de paquets de votre système pour installer GCstar (lancement proposé par le navigateur web ou le gestionnaire de fichiers) ou lancez une commande en mode ligne (**dpkg -i ...**, **dnf install ...**, ...).

Si un paquet correspondant à votre système n'est pas disponible, il faut faire une installation manuelle à partir des sources.

## Téléchargement et extractions des fichiers sources

Vous pouvez télécharger les sources de **GCstar** en utilisant les liens [1.8.0 version basée sur GTK3](https://gitlab.com/GCstar/GCstar/-/archive/v1.8.0/GCstar-v1.8.0.tar.gz) ou [1.7.2 version basée sur GTK2](https://gitlab.com/Kerenoc/GCstar/-/archive/v1.7.2/GCstar-v1.7.2.tar.gz).

Positionnez-vous dans le répertoire où vous avez sauvé l'archive et entrez: 

```bash
tar zxf GCstar-VERSION.tar.gz
```

 Un répertoire **gcstar** va être créé qui contient le script d'installation  **install**. Positionnez-vous dans ce répertoire. Vous avez le choix entre 3 types d'installation détaillées dans les paragraphes suivants.

Les procédures d'installation testent la disponibilité des bibliothèques dont dépend GCstar et s'arrête si un composant nécessaire est absent. Installez ces composants en utilisant les méthodes usuelles de votre distribution. L'étape suivante est le choix de l'endroit où installer GCstar (typiquement **/usr/bin** ou **/usr/local/bin**). Sont alors créés les répertoires suivants:

    Répertoire de base
        /bin
            Exécutable GCstar
        /lib
            /gcstar
                Scripts Perl de GCstar
        /share
            /gcstar
                Données internes utilisées par GCstar

 L'exécutable **gcstar** dans le répertoire **bin** pourra alors être lancé. Si votre variable d'environnement **PATH** pointe vers ce répertoire, vous pouvez entrer la commande :

```bash
gcstar
```

## Installation en mode graphique

Lancez la commande **install** sans paramètre : 

```bash
./install
```

La fenêtre suivante qui s'ouvre liste les dépendances résolues ou non 

![Installation dependencies](Dependencies.png)

Si des dépendances essentielles ne sont pas présentes, l'installation ne sera pas possible. Si des dépendances non essentielles ne sont pas présentes, certaines fonctionalités de GCstar ne seront pas disponibles.

Le bouton **Forward** donne accès au choix de la destination pour le répertoire de base puis à quelques paramètres.

## Installation en mode texte

Si **install** est lancé avec l'option **--text**, le mode texte est activé : 

```bash
./install --text
```

Le programme demande alors de préciser le répertoire où installer GCstar.

## Installation automatique

 Pour une installation en mode automatique, il faut préciser le répertoire de base comme paramètre de l'option **--prefix** du programme **install**. Par exemple :

```bash
./install --prefix=/usr/local
```

