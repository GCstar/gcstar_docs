+++
title = "Download and Installation"
description = ""
date = "2022-09-27T19:20:24+02:00"
weight = 2
+++

{{%notice info%}}The **GCstar** versions available in some Linux distributions repository are sometimes outdated. It thus may be necessary to do a manual install on these distributions. {{%/notice%}}

GCstar packages are available in the [Package Registry](https://gitlab.com/GCstar/GCstar/-/packages) of the Gitlab project. The latest version publiée is currently [1.8.0](https://gitlab.com/GCstar/GCstar/-/packages/13986875), the registry also host the packages for the development version.

# Windows 

Download the **.exe** and execute it on your computer to launch the install program.

# Linux

Check the [list of packaged GCstar versions](https://repology.org/project/gcstar/versions) to know if GCstar can be installed using the software manager of your distribution.

Otherwise, from the [Gitlab registry](https://gitlab.com/GCstar/GCstar/-/packages) download the package supported by your distribution if available (**.deb** pour Debian/Ubuntu et derivatives, **.rpm** pour Fedora et derivatives). Execute the software manager of your distribution to open this package and install GCstar (it may be propose directly by your browser after download or by the file manager). One may also use a command line (**dpkg -i ...**, **dnf install ...**, **apt install ...**,...).

If a package for your system is not available, you have to install GCstar from the sources.

## Download and extract GCstar's sources

You can download GCstar's source code through these links:

[1.8.0 version on GTK3](https://gitlab.com/GCstar/GCstar/-/archive/v1.8.0/GCstar-v1.8.0.tar.gz) or [1.7.2 version on GTK2](https://gitlab.com/Kerenoc/GCstar/-/archive/v1.7.2/GCstar-v1.7.2.tar.gz) (not maintained anymore).

Change to directory where this file has been saved and enter: 

```bash
tar zxf GCstar-VERSION.tar.gz
```

A **gcstar** directory will be created. It contains the installation script  **install**. Change to this directory. You now have to choose between three kinds of installation detailed in next paragraphs.

All installation processes check for the software's dependencies and stop the installation procedure if one is missing. Install them on you system to progress. Once done, you can then choose base directory for installation (typically **/usr/bin** or **/usr/local/bin**). You will have then this tree:

    Base directory
        /bin
            Main GCstar executable
        /lib
            /gcstar
                GCstar Perl scripts
        /share
            /gcstar
                Internal data used by GCstar

 The **gcstar** executable in **bin** directory can be launched. If your **PATH** variable contains this directory, just type in:

```bash
gcstar
```

## Graphic mode installation

 To proceed with a graphical installation, just launch **install** without any parameter. It requires a functioning **Perl+Gtk3** environment.

```bash
./install
```

You will then see a window with information about the status of GCstar's dependencies.

![Installation dependencies](Dependencies.png)

If some mandatory dependencies are missing, the installation won’t be possible. If missing, optional dependencies will disable some features of the program (web plugins, imports/exports). They will be listed with the name of the modules that require them.

Using the **Forward** button gives access to the base directory selection, and then to some settings. 


### Text mode installation

If parameter **--text** is given to **install** executable, the text mode installation will be used.

```bash
./install --text
```

The base directory has then to be specified.

## Automatic installation

 GCstar can be installed in an automatic way. The base directory must be passed to the **install** program using **--prefix** command line option. As an example:

```bash
./install --prefix=/usr/local
```


## Specific installations

There are convenient ways to install GCstar on some systems. There are described in other pages listed below. Most of the packages are external contributions. So they may be out of synchronization with last official one (the **.tar.gz** archive or the packages from Gitlab). 

* [GNU/Linux installation](Install_Linux) (contains instructions for several distributions)
* [FreeBSD installation](Install_FreeBSD)
* [Mac OS X installation](Install_MacOS)

___


