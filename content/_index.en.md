+++
title = "GCstar Documentation"
description = ""
+++

# Documentation GCstar
**GCstar** is a collection manager available on Linux, Windows and MacOS. It natively supports common types of collections (books, videos, comics, music, TV series, video games, board games, toys, miniature vehicules, stamps, coins, gadgets...) and can be customized to support new ones.

{{%panel%}} GCstar is an **open source** software written in Perl and based on the GTK graphical environment. It's available in a [Gitlab repository {{%icon fa-gitlab%}}](https://gitlab.com/GCstar/GCstar) {{%/panel%}}


## Main characteristics

* Customizable collections and interface
* Plugins to download information from the web
* Management of borrowed items
* Importation/exportation in various formats including HTML with styling, Tellico, DataCrow...
* Barcode scanning for books, gadgets, CDs, DVDs, BluRays

![Screenshot Ubuntu](/GCstar_1.8.0_Ubuntu.png)

![Screenshot Windows](/GCstar_1.8.0_Windows.png)


## Documentation sources

This documentation has been statically generated with [Hugo](https://gohugo.io/) from the sources in a [Gitlab repo {{%icon fa-gitlab%}}](https://gitlab.com/GCstar/GCstar_Docs) and is available as [Gitlab pages](https://gcstar.gitlab.io/gcstar_docs/en/).

Feel free to update this content :  just click the **Improve this page** link displayed at the bottom right of each page, fork, modify the relevant page and submit a merge request. When it is accepted, your modifications will be deployed automatically!