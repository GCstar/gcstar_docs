+++
title = "News"
description = "Lastest news on GCstar"
weight = 1
+++

# 2024/11/26 - New Gtk3 version on Windows

The installer for Windows is now based on version 3.24.43 for Gtk.

# 2024/04/09 - New packaging for Windows

The latest installer for Windows is based on version 5.38.2 for Perl and version 3.24.41 for Gtk.

# 2023/06/12 - Linux packages available

RPM and DEB packages for the latest development version can be downloaded from the [Gitlab Packages Registry](https://gitlab.com/GCstar/GCstar/-/packages/13986875).

For Debian or Ubuntu based distributions, a PPA (Personal Package Archive) of the latest development version is available on [Launchpad](https://code.launchpad.net/~gcstar/+archive/ubuntu/gcstar-dev).

# 2023/04/19 - 1.8.0 version released

The [1.8.0 version](https://gitlab.com/GCstar/GCstar/-/releases/v1.8.0) marks the end of the migration to newer versions of Gtk3 (most notably on Windows with Gtk 3.24). This long due version include all the evolutions and bug fixed of the last months (rather years!). For more details, please read the [Changelog](https://gitlab.com/GCstar/GCstar/-/blob/main/gcstar/CHANGELOG).

Warning : the models for the Stamps and Periodicals were changed to support additional user defined fields. The collections in the old format should be automatically migrated when opened but it is recommended to make backups before using the new GCstar version and to check the collections' content before saving them.

# 2022/08/25 - Problems with the web site

Due to cyber attacks the [www.gcstar.org](http://www.gcstar.org) was closed by Tian, the original author of GCstar. The GCstar forum was also closed for the same reasons.

# 2022/06/01 - Open source program by Gitlab

GCstar is benefiting from the open source program by Gitlab, including free quotas to use the CI/CD process on Gitlab.com

# 2022/04/04 - Move from Gtk2 to Gtk3

On the Gitlab repository, the main branch is now based on GTk3, the Gtk2 version being developped in the Gtk2 branch. Whenever possible, bug fixes, plugin evolution and new functionalities will be backported to the Gtk2 version.

# 2019/12/09 - Version 1.7.3

This is the first version based on Gtk3, to be used and tested.

# 2018/06/09 - Version 1.7.2

This is the first version since the migration from GNA to Gitlab.com. An installer for Windows is available as well as packages for Linux.

# 2016/03/09 - Version 1.7.1

This was the last version published by Tian on gna.org.

