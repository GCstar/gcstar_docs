+++
title = "Actualités"
description = "Fil d'actualités sur GCstar"
weight = 1
+++

# 2024/11/26 - Nouvelle version de GTK sur Windows

L'installeur Windows est basé sur la version 3.24.43 de Gtk.

# 2024/04/09 - Nouveau package pour Windows

Le dernier package pour Windows est basé sur la version 5.38.2 de Perl et la version 3.24.41 de Gtk.

# 2023/06/12 - Disponibilité de paquets Linux

Des paquets RPM et DEB packages pour la version en cours de développement sont disponibles sur le [registre de paquets Gitlab](https://gitlab.com/GCstar/GCstar/-/packages/13986875).

Pour les distributions basées sur Debian or Ubuntu, un PPA (Personal Package Archive) de la version en cours de développement est disponible sur [Launchpad](https://code.launchpad.net/~gcstar/+archive/ubuntu/gcstar-dev).

# 2023/04/19 - Publication de la version 1.8.0

La [version 1.8.0](https://gitlab.com/GCstar/GCstar/-/releases/v1.8.0) entérine la migration de GCstar vers les dernières versions de GTK3 notamment sur Windows (3.24). Elle intègre toutes les modifications et corrections des derniers mois, voir années! Pour plus d'informations, consulter le fichier [CHANGELOG](https://gitlab.com/GCstar/GCstar/-/blob/main/gcstar/CHANGELOG)).

Attention : les modèles pour les collections de timbres et de revues ont été modifiés pour permettre l'ajout de champs supplémentaires par les utilisateurs. Les collections utilisant l'ancien format sont migrées automatiquement lors de leur ouverture : il faut toutefois bien en vérifier le contenu avant de les enregistrer (une sauvegarde de secours est recommandée au préalable).

# 2022/08/25 - Fermeture du site web

Suite à des cyber-attaques, le site web [www.gcstar.org](http://www.gcstar.org) et le forum associé ont été fermés par Tian, le créateur de GCstar. 

# 2022/06/01 - Labellisation Gitlab

GCstar a été labellisé comme programme logiciel libre par Gitlab.com ce qui permet de bénéficier de quotas supplémentaires pour l'utilisation des processus d'intégration continue CI/CD.

# 2022/04/04 - Migration de Gtk2 vers Gtk3

Dans le dépôt Gitlab, la branche principale **main** est maintenant basée sur Gtk3. La version Gtk2 est maintenue dans la branche Gtk2. Autant que possible, les corrections d'erreurs, les évolutions des extensions et les nouvelles fonctionalités seront rétroportées sur la version Gtk2.

# 2019/12/09 - Version 1.7.3

Le portage sur Gtk3 est majoritairement terminé. Cette version permet de la tester et de l'utiliser.

# 2018/06/19 - Version 1.7.2

Cette version est la première depuis l'arrêt de l'hébergement sur GNA et l'utilisation de Gitlab. Un installeur Windows est disponible ainsi que des packages Linux.

# 2016/03/09 - Version 1.7.1

Cette version est la dernière publiée par Tian et hébergée sur gna.org. 
