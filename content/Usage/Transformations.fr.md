+++
title = "Transformations"
description = "Transformations"
weight = 4
+++

* [Généralités]({{< ref "#generalites">}})
* [Exportation des collections vers d'autres formats]({{< ref "#export">}}) :
    [CSV]({{< ref "#exportCSV">}}) ,
    [HTML]({{< ref "#exportHTML">}}) ,
    [SQL]({{< ref "#exportSQL">}}) ,
    [.tar.gz]({{< ref "#exportTGZ">}}) ,
    [Tellico]({{< ref "#exportTC">}}) ,
    [XML]({{< ref "#exportXML">}})
* [Importation depuis d'autres logiciels]({{< ref "#import">}}) :
    [Alexandria]({{< ref "#importAlexandria">}}) ,
    [Ant Movie Catalog]({{< ref "#importAMC">}}) ,
    [CSV]({{< ref "#importCSV">}}) ,
    [DVD Profiler]({{< ref "#importDVD">}}) ,
    [GCfilms]({{< ref "#importGCF">}}) ,
    [Liste de noms]({{< ref "#importList">}}) ,
    [.tar.gz]({{< ref "#importTGZ">}}) ,
    [Tellico]({{< ref "#importTC">}})

Il est possible d’utiliser GCstar avec d’autres logiciels en transformant les collections de deux manières. L’exportation est l’opération permettant de rendre une collection exploitable depuis un autre logiciel. L’importation réalise l’inverse et vous permettra par exemple de récupérer une collection créée avec un autre logiciel.

# Généralités {#generalites}

Les modules sont décrits dans l’ordre où ils devraient apparaître dans les menus Exporter et Importer (sous-menus de Fichier). Si un de ceux-ci est absent, il est possible de savoir pourquoi en utilisant l’élément Dépendances dans le menu d’aide (?). Il affiche une boîte de dialogue ayant cet aspect :

![Dépendance des modules](/Usage/Dependances.png)

Les éléments montrés ici sont des composants Perl qui sont utilisés par les modules d’importation et d’exportation. Si un de ceux-ci est absent du système, le module l’utilisant ne sera pas disponible. Dans l’exemple précédent, le module d’importation depuis Tellico ne pourra pas être utilisé et ne s’affichera pas dans le menu Importer car Archive::Zip est manquant.

Une fois le composant Perl ajouté (la manière de procéder dépend du système d’exploitation utilisé), il n’est pas nécessaire d’installer à nouveau GCstar, mais il suffit de le quitter puis de le relancer.

Certains modules sont aussi spécifiques à un certain de type de collection ou se comporteront différemment selon la collection concernée.

# Exporter les collections {#export}

Le menu Fichier a un sous-menu appelé Exporter. Il contient plusieurs éléments correspondant à des types d’exportation différents.

## Exporter en CSV {#exportCSV}

Le premier module sert à créer un fichier **CSV** (**C**omma **S**eparated **V**alues). Dans un tel fichier, il y a une ligne pour chaque membre de la collection. Les informations associées sont séparées par un caractère (ou une chaîne de caractères) défini par l’utilisateur.

La première option permet de choisir si les noms des colonnes doivent être ajoutées au fichier. Ce peut être utile de les avoir pour par exemple ensuite exploiter cette liste depuis un tableur.

Il est ensuite possible de choisir le délimiteur. Et au cas où ce dernier apparaîtrait dans une des valeurs à insérer, un caractère de substitution doit être indiqué.

Une autre option permet de choisir si les images doivent être copiées dans un sous-répertoire. Les chemins seront par la même occasion changés pour les rendre relatifs.

Les champs à exporter doivent être sélectionnés à l’aide du bouton présent. L’ordre a bien évidemment une importance dans ce cas.

## Exporter en HTML {#exportHTML}

Grâce à cette fonctionnalité, une page **HTML** (**H**yper**T**ext **M**arkup **L**anguage) est générée contenant toute la collection courante avec leurs images. Pour stocker ces dernières, un répertoire est créé au même niveau que le fichier. Son nom est affiché dans une boîte de dialogue lorsque l’exportation est terminée. Pour pouvoir consulter cette page depuis un autre endroit (par exemple un serveur web), ce répertoire doit aussi être copié.

Plusieurs modèles sont disponibles avec des fonctionnalités et des apparences différentes. Les modèles disponibles seront différents selon le type de collection exportée.

Une option **Utiliser du Javascript** est également présente  qui permet d'ajouter certaines fonctionnalités dynamiques à la page HTML.

D’autres personnalisations sont possibles. Le titre de la page générée est modifiable par l’utilisateur. La taille des images peut également être spécifiée.

## Exporter en SQL {#exportSQL}

L’export **SQL** (**S**tructured **Q**uery **L**anguage) permet de créer un fichier contenant des requêtes d’insertion des élements dans une table d'une base de données relationnelle. L’utilisateur doit indiquer le nom de la table en question, mais il y a aussi d’autres options.

Deux options servent à indiquer si des instructions doivent être placées avant les insertions. Il s’agit d’une destruction préalable de la table (**DROP**) et de sa création (**CREATE**).

L’option suivante permet d’indiquer s’il est nécessaire que les images associées soit copiées dans un sous-répertoire (qui sera créé) de celui où va être enregistré le fichier SQL.

Le bouton se trouvant en dessous de ces options sert à spécifier quels champs doivent être exportés dans le fichier. Leur ordre est également modifiable à ce niveau.

## Exporter en .tar.gz {#exportTGZ}

Ceci permet de générer un fichier unique, une archive compressée .tar.gz, qui contiendra le fichier de la liste mais aussi toutes les images. C’est utile pour par exemple transporter facilement une collection d’une machine vers une autre.

## Exporter pour Tellico {#exportTC}

Ce module permet d’exporter la collection afin qu’elle puisse être lue par **Tellico**, un gestionnaire de collections pour KDE.

La collection est crée sous forme d’un fichier XML unique contenant également les images. Les collections de films, jeux vidéo, livres, musiques ou pièces peuvent être exportées vers Tellico.

## Exporter en XML {#exportXML}

L’export **XML** (e**X**tensible **M**arkup **L**anguage) utilise des modèles pour générer le fichier. Le premier choix permet de selectionner où ce modèle est situé. Il peut être entré manuellement dans la zone de texte ou lu dans un fichier. GCstar est également fourni avec des modèles pré-définis (selon le type de collection) qui peuvent être utiles.

Un modèle contient trois sections définies comme suit.

````
[HEADER]

[/HEADER]
[ITEM]

[/ITEM]
[FOOTER]

[/FOOTER]
````

Dans la section **HEADER** doit se trouver tout ce qui sera au début du fichier. Ces informations seront présentes seulement une fois. Ce sera par exemple la définition de la DTD, l’ouverture des balises principales,...

Ce qui est dans la partie **ITEM** est ce qui se rapporte à un membre de la collection. Ce sera répété autant de fois que nécessaire.

Enfin, la section **FOOTER** regroupe ce qui sera ajouté en fin de fichier.

Dans la section **ITEM**, des opérations particulières peuvent être réalisées. Quelque chose de la forme **${champ}** sera remplacé par le champ correspondant dans l’élément. Les valeurs de champs possibles dépendent du type de collection.

Toutes les valeurs séparées par des virgules ou les listes de valeurs peuvent être utilisées dans une boucle. La syntaxe est la suivante :

````
[LOOP champ]

[/LOOP]
````

**champ** étant comme précédemment (sans **${** et **}** ). Tout le texte inclus dans cette section sera répété autant de fois que nécessaire. A chaque itération de la boucle, **$$** sera remplacé par la valeur courante. Voici un exemple. Supposons que ce modèle soit utilisé pour une liste de films.
````
<acteurs>
[LOOP actors]
  <acteur>$$</acteur>
[/LOOP]
</acteurs>
````
Et qu’un film ait pour liste d’acteurs “Jacques, Francine, Paul”. Le fichier généré contiendra :
````shell
<acteurs>
  <acteur>Jacques</acteur>
  <acteur>Francine</acteur>
  <acteur>Paul</acteur>
</acteurs>
````
Une autre fonctionnalité est la section SPLIT. Sa syntaxe est la suivante.
````
[SPLIT value=champ sep=car]

[/SPLIT]
````
**champ** peut être n’importe laquelle des informations précédentes (pas seulement celles qui ne sont pas séparées par des virgules). **car** est le caractère qui sera utilisé en tant que séparateur. Dans le texte inclus dans cette section, des valeurs particulières peuvent être utilisées : **$0**, **$1**,... Elles seront remplacées par chacun des champs.

Pour prendre un exemple, considérons ce modèle :
````
<realisateur>
[SPLIT value=director sep= ]
  <prenom>$1</prenom>
  <nom>$2</nom>
[/SPLIT]
</realisateur>
````
Le séparateur utilisé ici est l’espace. Si pour un film le réalisateur est “Paul Martin”, alors le fichier contiendra ceci :
````shell
<realisateur>
  <prenom>Paul</prenom>
  <nom>Martin</nom>
</realisateur>
````

# Importation depuis d'autres logiciels {#import}

GCstar peut être utilisé avec des collections générées depuis d’autres logiciels. Il existe des modules d’importation pour cela. La collection peut être importée dans celle courante (si elles sont de types identiques) ou il est possible d’en créer une nouvelle. Cela se choisit dans la boîte de dialogue d’importation. Il y a plusieurs importations possibles.

## Alexandria {#importAlexandria}

Ce module permet l’import de collections de livres générées par **Alexandria**, un gestionnaire de collections de livres pour Gnome. Vous pouvez laisser GCstar rechercher dans l’emplacement utilisé par défaut par Alexandria ou spécifier un chemin particulier.

## Ant Movie Catalog {#importAMC}

Ce module permet d’importer une liste de films générée avec **Ant Movie Catalog**. Il suffit de choisir le fichier .amc et tout sera fait, y compris l’importation des images de films.

## CSV {#importCSV}

De nombreux logiciels permettent d’exporter la liste au format CSV. Pour pouvoir importer une telle liste dans GCstar, il faut utiliser ce module et spécifier le caractère utilisé comme séparateur ainsi que les différents champs présents.

## DVD Profiler  {#importDVD}

Avec ce module, ce sont les collections utilisées avec **DVD Profiler** qui peuvent être importées. Il ne fonctionne pas avec le format natif mais avec les fichiers XML générés par ce logiciel. Il faut donc tout d’abord utiliser la fonction d’exportation qu’il propose. A noter que cette importation ne fonctionne qu’avec les fichiers .xml générés par ce logiciel. En effet, l’exportation en .xml par le logiciel **movie collection** ne fonctionne pas.

## GCfilms {#importGCF}

Cela permet de récupérer une liste de films créée avec **GCfilms**, le précurseur de GCstar.

## Liste de noms {#importList}

Avec ce module, on peut importer une liste de noms. Cette liste doit être dans un fichier texte avec un élément par ligne. Il est ensuite possible de choisir depuis quel site les informations vont être automatiquement récupérées.

Parfois le nom peut donner plusieurs résultats sur le site de recherche. Le comportement dans ces cas là peut être indiqué par la case à cocher présente. Si elle est cochée, c’est systématiquement le premier résultat qui sera utilisé sans le demander à l’utilisateur. Si elle n’est pas cochée, l’utilisateur se verra demander quel élément il veut choisir dans une liste.

## .tar.gz {#importTGZ}

Cette importation permet de récupérer une collection qui a été générée par le module d’exportation correspondant. Il s’agit alors d’une archive compressée contenant la liste elle-même et les images.

## Tellico {#importTC}

Ce module sert à importer des collections depuis **Tellico**, un gestionnaire de collections pour KDE.

Il sait ouvrir les deux types de collections utilisés par Tellico : Les fichiers .tc et les fichiers XML. Il n’importera que les collections qui sont de type films, jeux vidéo, livres, musiques et pièces.
