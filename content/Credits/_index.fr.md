+++
title = "Crédits"
description = "Attributions pour le développement de GCstar et sa documentation"
weight = 7
+++

{{%alert%}}Merci à tous les contributeurs qui ont fait évoluer GCstar!{{%/alert%}}

# Développeurs

* Créateur de GCstar : [Tian (Christian Jodar)](http://www.c-sait.net/)
* Mainteneur actuel : [Kerenoc](https://gitlab.com/Kerenoc)

{{%glcontributors "https://gitlab.com/api/v4/projects/2167651/repository/contributors" %}}


# Plugins

* Christian Jodar
* Tian
* Zombiepig
* Florent
* Varkolak
* Knight Rider
* Jonas/jojotux
* TPF
* MW
* Jpa31
* Uncle Petros
* BW
* Nure
* Michel_P
* Mckmonster
* Zserghei
* FiXx
* MeV
* PIN
* T-storm
* Groms
* Snaporaz
* 2emeagauche
* P Fratczack
* MaTiZ
* Zuencap
* Nazaro Pavel
* Marek Cendrowicz
* DoVerMan
* BigGriffon
* Gimont

# Documentation

* Christian Jodar
* Kérénoc

# Traductions

* Christian Jodar

# Création artistique

* [Le Spektre](https://www.le-spektre.org/) : logo et design web
* Łukasz Kowalczk (Qoolman) : thèmes graphiques

# Bibliothèques et packages

* [Perl](https://www.perl.org) - langage de programmation généraliste
* [Gtk](http://www.gtk.org/) - environnement graphique pour les interfaces utilisateur
* packages Perl (liste disponible dans GCstar Menu Aide/Dépendances)


# Outils

* [Gitlab](https://www.gitlab.com) - dépôt Git et intégration continue
* [Hugo](https://gohugo.io/) {{%icon "fa-smile-o"%}} - générateur de site web statiques
* [DocDock](https://github.com/vjeantet/hugo-theme-docdock) - thème Hugo dédié à la documentation
* [NSIS](https://nsis.sourceforge.io) - créateur du programme d'installation sur Windows
