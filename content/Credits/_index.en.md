+++
title = "Credits"
description = "contributors and packages"
weight = 7
+++

{{%alert%}}Thanks to all contributors for helping make GCstar better!{{%/alert%}}

# Developpers

* GCstar creator : [Tian (Christian Jodar)](http://www.c-sait.net/)
* Current maintainer : [Kerenoc](https://gitlab.com/Kerenoc)

{{%glcontributors "https://gitlab.com/api/v4/projects/2167651/repository/contributors" %}}

# Plugins

* Christian Jodar
* Tian
* Zombiepig
* Florent
* Varkolak
* Knight Rider
* Jonas/jojotux
* TPF
* MW
* Jpa31
* Uncle Petros
* BW
* Nure
* Michel_P
* Mckmonster
* Zserghei
* FiXx
* MeV
* PIN
* T-storm
* Groms
* Snaporaz
* 2emeagauche
* P Fratczack
* MaTiZ
* Zuencap
* Nazaro Pavel
* Marek Cendrowicz
* DoVerMan
* BigGriffon
* Gimont

# Documentation

* Christian Jodar
* Kerenoc

# Translations

* Christian Jodar

# Artwork

* [Le Spektre](https://www.le-spektre.org/) : log and web design
* Łukasz Kowalczk (Qoolman) : skins

# Packages and libraries
* [DocDock](https://github.com/vjeantet/hugo-theme-docdock) - Hugo theme derived for the documentation
* [Perl](https://www.perl.org) - general purpose programming language
* [Gtk](http://www.gtk.org/) - graphical environment for the UI


# Tooling

* [Gitlab](https://www.gitlab.com) - Git repository and CI/CD 
* [Hugo](https://gohugo.io/) {{%icon "fa-smile-o"%}} - static web site generator
* [Ant](https://ant.apache.org) - build tool
* [NSIS](https://nsis.sourceforge.io) - installer creator on Windows
