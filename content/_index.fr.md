+++
title = "Documentation GCstar"
description = ""
+++

# Documentation GCstar
**GCstar** est un gestionnaire de collection disponible sur les systèmes d'exploitation Linux, Windows et MacOS. Par défaut, il peut gérer des collections standard : livres, films, bandes dessinées, vidéos, musique, séries télévisées, jeux vidéo, jeux de plateau, jouet, véhicules miniature, timbres, pièces, matériel...) et peut être paramétré pour gérer des items nouveaux.

{{%panel%}} GCstar est un **logiciel libre** écrit en Perl et basé sur l'environmment graphique GTK. Il est disponible dans un [dépot Gitlab {{%icon fa-gitlab%}}](https://gitlab.com/GCstar/GCstar) {{%/panel%}}


## Caractéristiques principales

* Interface et collections paramétrables
* Extensions (plugins) pour télécharger des informations depuis le web
* Gestion des items empruntés
* Importation/exportation dans divers formats y compris HTML avec des styles modifiables, Tellico et DataCrow...
* Scan de codes barres pour les livres, gadgets, CDs, DVDs, BluRays

![Screenshot Ubuntu](/GCstar_1.8.0_Ubuntu.png)

![Screenshot Windows](/GCstar_1.8.0_Windows.png)


## Sources de la documentation

Cette documentation est générée avec  [Hugo](https://gohugo.io/) à partir de fichiers sources dans un [dépôt Gitlab {{%icon fa-gitlab%}}](https://gitlab.com/GCstar/GCstar_Docs) et est disponible sous forme de [pages Gitlab](https://gcstar.gitlab.io/gcstar_docs/fr/).

N'hésitez pas à en compléter le contenu ou à l'améliorer :  cliquez sur le lien **Modifier** en bas à droite de chaque page, "forker", faites vos modifications puis soumettez un "merge request" sur Gitlab. La documentation sera modifiée automatiquement après l'acceptation de votre soumission !

