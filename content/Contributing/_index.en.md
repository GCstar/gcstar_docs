+++
title = "Contributing"
description = "How to contribute to the GCstar project"
weight = 4
+++

All contributions are welcome : Toute contribution est la bienvenue : [signaling issues]({{< ref "#errors">}}), [testing new versions]({{<ref "#tests">}}) [source code evolutions]({{<ref "#code">}}), [extending the program]({{<ref "#extensions">}}), [web plugins]({{<ref "#plugins">}}), [collection types]({{<ref "#models">}}), [reports]({{<ref "#reports">}}), [theming]({{<ref "#themes">}}),  [packaging]({{<ref "#packaging">}}), [translations]({{<ref "#translation">}}), [documentation]({{<ref "#documentation">}}), [promotion]({{<ref "#promotion">}}).

# Reporting bugs {#errors}

If you find some inconsistency or error while using GCstar, you can check the [reported issues](https://gitlab.com/GCstar/GCstar/-/issues) on GitLab to see if the problem had already be indentified. If not, you can create a new issue to describe your findings (it requires a Gitlab account). pour voir si cela a déjà été signalé. Si ce n'est pas le cas, vous pouvez créer un nouveau ticket pour décrire ce que vous avez constaté (il faut posséder un compte GitLab).

You may also create an issue to ask for help or present your ideas for new features.

Alternatively, you may visit the [dedicated Discord channel](https://discord.com/channels/1087752972995395654/1087752973712625696).


# Testing development versions {#tests}

GCstar is continuously evolving between released version that freeze some stabilized functionalities and may be packaged for common environments. You can install the lastest development using the [latest packages](https://gitlab.com/GCstar/GCstar/-/packages/13986875) (see the [Download and Installation page]({{%ref "Download_Installation" %}})). You can also update a functioning installation of GCstar to get the lastest scripts and models with `gcstar -u` (beware that it's not easily reversible).

Remember to regularly make backups of your GCstar collections, all the more so before using a development version of the program. 

Additional caution : you can install GCstar in a different folder from you usual install (for exemple **GCstar_Tests**) and  and tell GCstar not to overwrite the existing configuration files:
````bash
cd GCstar_Tests
mkdir config
export XDG_CONFIG_HOME=`pwd`/config
````

You can have more warning messages while executing GCstar to help track down issues.
````bash
cd GCstar_Tests
perl -W bin/gcstar
````

Note : sur Windows, use **wperl.exe** instead of **perl** and get the warning messages in the **%HOMEPATH%\AppData\Roaming\gcstar.log** file.



# Contribution to the source code {#code}

The lastest development version is on the [main branch](https://gitlab.com/GCstar/GCstar/-/tree/main) of the Gitlab repository. If you want to participate to the project, you can check the unsolved [issues](https://gitlab.com/GCstar/GCstar/-/issues). You can post a comment saying that you're begin to work on the issue. At any time, you can use comments associated with issues to discuss or ask to help from the other contributors.

The preferred process is : 
* [forking](https://gitlab.com/GCstar/GCstar/-/forks/new) on GitLab to create you own project
* cloning it locally to start working on the code (`git clone`)
* modify the files (mainly Perl script) corresponding to the target issue or extension 
* test the modifications (sorry, no unit testing yet on the project)
* do a `git commit` with a message giving some cues on the changes, prefixing it with the functional domain that is impacted.For consistency, try to imitate the previous commits (`git log --pretty=format:"%h%x09%an%x09%ad%x09%s" --date=iso --no-merges`). You can add a line to the commit message with the URI of the Gitlab issue as a cross-reference. 
* push the changes to Gitlab (`git push`)
* do a **merge request** that will be reviewed before integration to the **main** branch.

A simplier by riskier process is to directly modify the files of an installed version of GCstar (after updating it with `gcstar -u`) then to post the changes you made (the modified files or a `diff`) in a forum, as an attachment of an issue or by mailing it to **kerenoc01** on Google mail).

## Extensions {#extensions}

### Import/Export modules {#importexport}

### Web plugins {#plugins}

### Collection models {#models}

### Report models {#reports}

## Themes {#themes}

## Packaging {#packaging}

# Translations {#translation}

# Documentation {#documentation}

# Promotion {#promotion}




