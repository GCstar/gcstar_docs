+++
title = "Contribuer au projet"
description = "Comment contribuer au projet ou sa documentation"
weight = 4
+++

Toute contribution est la bienvenue : [signalement d'erreurs]({{< ref "#errors">}}), [test des nouvelles versions]({{<ref "#tests">}}) [contribution au code]({{<ref "#code">}}), [écriture d'extensions pour utiliser des sites web ou faire des importations/exportations]({{<ref "#extensions">}}), [ajout de nouveaux types de collection]({{<ref "#models">}}), [création de nouveaux rapports]({{<ref "#reports">}}), [amélioration de l'apparence]({{<ref "#themes">}}), [contribution aux traductions]({{<ref "#translation">}}), [amélioration de la documentation]({{<ref "#documentation">}}), [promotion]({{<ref "#promotion">}}).

{{%notice note%}}La priorité actuelle est de mettre à jour les [**packages GCstar**](https://repology.org/project/gcstar/versions) pour un maximum de distributions afin de faciliter son installation.{{%/notice%}}

# Signalement d'erreurs {#errors}

Si vous détectez une incohérence, un dysfonctionnement ou une erreur lors de l'utilisation de GCstar, vous pouvez consulter les [tickets](https://gitlab.com/GCstar/GCstar/-/issues) sur GitLab pour voir si cela a déjà été signalé. Si ce n'est pas le cas, vous pouvez créer un nouveau ticket pour décrire ce que vous avez constaté (il faut posséder un compte GitLab).

Vous pouvez aussi créer un ticket pour demander de l'aide ou proposer des évolutions fonctionnelles.

Les demandes sur GCstar peuvent aussi se faire sur [Discord](https://discord.com/channels/1087752972995395654/1087752973712625696).

# Tests de la version de développement {#tests}

GCstar est en mode de développement continu entre les versions qui gêlent un ensemble de fonctionalités stabilisées. La version de développement est disponible dans le [registre de paquets](https://gitlab.com/GCstar/GCstar/-/packages) sur GitLab. 

N'oubliez pas de faire des sauvegardes de vos collections auparavant (à faire régulièrement en général pour se prémunir non seulement d'erreurs logicielles mais aussi de pannes matérielles!).

Précaution supplémentaire : vous pouvez installer GCstar dans un répertoire différent de la version courante par exemple **GCstar_Tests** et indiquer à GCstar de ne pas écraser les fichiers de configuration existants:
````bash
cd GCstar_Tests
mkdir config
export XDG_CONFIG_HOME=`pwd`/config
````

Si Perl est installé dans votre environnement, vous pouvez exécuter GCstar en affichant les messages d'avertissement pour compléter vos rapports d'erreur.
````bash
cd GCstar_Tests
perl -W bin/gcstar
````

Note : sur Windows, ces messages d'avertissement sont dans un fichier **%HOMEPATH%\AppData\Roaming\gcstar.log**


# Contribution au code {#code}

Si vous voulez participer au développement, vous pouvez consulter la liste des [tickets](https://gitlab.com/GCstar/GCstar/-/issues) non résolus. Ajoutez simplement un commentaire pour signaler que vous commencez à vous en occuper (pour éviter les doublons). A tout moment, vous pouvez utiliser les commentaires pour demander de l'aide aux autres développeurs.

Le plus "professionnel" est de 
* faire un [fork](https://gitlab.com/GCstar/GCstar/-/forks/new) sur GitLab pour créer votre propre projet
* faire un clone local de ce projet sur votre machine de développement (`git clone`)
* modifier les fichiers Perl correspondant à la fonctionalité désirée
* tester la fonctionalité en exécutant la version modifiée de GCstar 
* faire un `git commit` avec un message indiquant la modification faite en le préfixant par le périmètre fonctionnel impacté (faire un "git log" pour avoir des exemples de messages) et en ajoutant éventuellement une ligne supplémentaire pour indiquer l'URL du ticket concerné sur GitLab)
* pousser la modification sur GitLab (`git push`)
* faire une demande de fusion ("merge request") sur Gitlab qui sera examinée avec attention.

Le plus simple mais le plus risqué est de modifier directement les fichiers de votre version de GCstar (après avoir faire des sauvegardes et fait une mise à jour de GCstar avec `gcstar -u`) puis de communiquer les modifications effectuées (dans un forumn, en pièce jointe d'un ticket ou par envoi à kerenoc01 sur le mail Google).


## Extensions {#extensions}

Il y a plusieurs types d'extensions possibles GCstar, utilisables pour enrichir le programme ou l'adapter à des besoins nouveaux. Cela nécessite le plus souvent l'utilisation du langage Perl mais il est généralement possible de partir d'une extension existante. 

### Modules d'importation ou d'exportation {#importextport}

Ces modules servent à importer ou exporter des données depuis ou vers d'autres logiciels. Pour plus d'informations à ce sujet et avoir des exemples de modules existants, il est possible de consulter la [documentation sur les transformations]({{<ref "Usage/Transformations" >}}).

Implémenter un tel module revient à créer une classe Perl. Pour avoir un modèle, avec plus d'informations sur ce qu'il est nécessaire de faire, il faut télécharger les sources de GCstar (ficher .tar.gz). Une fois celles-ci décompressées, un répertoire templates contiendra ces modèles.

Pour pouvoir tester le module créé, il faut le copier dans le répertoire **lib/gcstar/GCImport** ou **lib/gcstar/GCExport** selon le cas et redémarrer GCstar. Ces répertoires sont donnés relativement à celui où est installé GCstar. Dans ces mêmes répertoires se trouvent des fichiers qui peuvent servir d'exemples.

### Modules de recherche Internet {#plugins}

Ils servent à rechercher sur des sites web les informations correspondantes à un élément d'une collection. afin de remplir automatiquement ces champs.

Pour les implémenter, on procédera de même que pour les modules d'importation ou d'exportation. Un modèle est également disponible. Le répertoire où copier le fichier sera dans ce cas **lib/gcstar/GCPlugins/TypeDeCollection**, en remplaçant **TypeDeCollection** par le nom adéquat commençant par **GC** (par exemple GCfilms ou GCgames) car ces modules sont spécifiques au type de collection. Il y a plus d'informations dans la page dédiée à l'écriture de modules de recherche Internet.

Si vous le souhaitez, vous pouvez consulter la [liste des modules que des utilisateurs ont demandés](https://gitlab.com/GCstar/GCstar/-/issues/?sort=updated_desc&state=opened&label_name[]=Plugins).

### Modèles de collection {#models}

Les modèles de collection sont des fichiers XML qui permettent de définir les champs permettant de représenter les différents items d'une collection d'un nouveau type. GCstar permet de les créer directement en utilisant l'interface graphique (**Menu Fichier/Nouveau/Nouveau type de collection**). 

### Modèles de rapports {#reports}

Ces modèles sont utilisés lors de l'exportation HTML ou XML pour générer le fichier de sortie. Plus d'informations à leur sujet peut être trouvé dans la documentation sur les transformations.

Les modèles doivent être copiés dans **share/gcstar/html_models/TypeDeCollection** ou **share/gcstar/xml_models/TypeDeCollection** où se trouvent des exemples pouvant servir de base. Là encore **TypeDeCollection** doit être remplacé par la bonne valeur, ces modèles étant spécifiques par type de collection.

## Thèmes {#themes}

Les thèmes permettent de changer l'interface graphique de GCstar. Ils utilisent les format standard Gtk3 pour les fichiers de ressources.

Des exemples peuvent être trouvés dans **share/gcstar/style**.

## Packaging {#packaging}

Rendre GCstar facilement installable sur les différents types d'environnements est un challenge. Toute aide pour le faire accepter dans les dépôts officiels ou non officiels des principaux systèmes d'exploitation est la bienvenue. 

# Traduction {#translation}

Vous pouvez trouver des informations à ce sujet dans la page concernant les traductions de GCstar dans d'autres langues.

# Documentation {#documentation}

La documentation fait l'objet d'un dépot Gitlab dédié [GCstar_Docs](https://gitlab.com/GCstar/gcstar_docs). Elle est générée par le logiciel [Hugo](https://gohugo.io) à partir de pages rédigées en utilisant le format Markdown puis mise en ligne à l'aide de [GitLab Pages](https://docs.gitlab.com/ce/user/project/pages/).

Pour proposer des améliorations if faut :

* faire un [fork](https://gitlab.com/GCstar/GCstar_Docs/-/forks/new) du projet sur Gitlab
* installer `git` et `go`.
* installer [Hugo](https://gohugo.io/getting-started/installing/).
* lancer un serveur local pour érifier la doc en consultant la [page d'accueil](http://localhost:1313/gcstar_docs/fr)
```bash
hugo server
```
Après avoir fait des modifications, les proposer à l'aide d'un `merge request` sur GitLab

# Promotion {#promotion}

Comme tous les projets open-source, la diffusion est un facteur important qui permet d'attirer des utilisateurs mais aussi de collecter des idées d'évolution et de susciter des contributions. Toute action de communication est la bienvenue : citation, articles de blog, post sur des forums et réseaux sociaux.
