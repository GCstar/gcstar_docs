# Contributing to the GCstar documentation

Fork this repository.

Learn [Hugo](https://gohugo.io), the static web framework used to build the pages with markdown.

Learn more about GitLab Pages at the [official documentation](https://docs.gitlab.com/ce/user/project/pages/)

## Building the doc locally

To work locally with this project, you'll have to follow the steps below:

1. Fork, clone or download this project.
1. Install `git` and `go`.
1. [Install](https://gohugo.io/getting-started/installing/) Hugo.
1. Install the theme as a Hugo module:

   ```shell
   hugo mod init gitlab.com/pages/hugo
   hugo mod get -u github.com/theNewDynamic/gohugo-theme-ananke
   ```

1. Preview your project:

   ```shell
   hugo server
   ```
   or
   ```shell
   alias hs='hugo server --buildFuture --minify --buildDrafts --disableFastRender'
   hs
   ```

1. Add content.
1. Optional. Generate the website:

   ```shell
   hugo
   ```

Read more at Hugo's [documentation](https://gohugo.io/getting-started/).

